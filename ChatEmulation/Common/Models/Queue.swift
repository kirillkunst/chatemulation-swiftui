//
//  Queue.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation

struct Queue<T> {
    var items: [T] = [T]()
    
    mutating func enqueue(element: T) {
        items.append(element)
    }
    
    mutating func dequeue() -> T? {
        if items.isEmpty {
            return nil
        }
        
        let tempElement = items.first
        items.remove(at: 0)
        return tempElement
    }
}
