//
//  Theme.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

extension Color {
    static var customBackground = Color(hex: "#F9FAFB")
    static var bubbleBackground = Color(hex: "#FDFDFE")
    static var shadowColor = Color.black
}

extension UIColor {
    static var customBackground = UIColor(hex: "#F9FAFB")
}
