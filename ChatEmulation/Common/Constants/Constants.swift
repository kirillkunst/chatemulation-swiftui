//
//  Constants.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation

struct Constants {
    static let delay: TimeInterval = 0.5
}
