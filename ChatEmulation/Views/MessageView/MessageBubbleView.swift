//
//  BubbleView.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct MessageBubbleView: View {
    
    var body: some View {
        RoundedRectangle(cornerRadius: 5)
        .foregroundColor(Color.bubbleBackground)
        .shadow(color: Color.shadowColor.opacity(0.5), radius: 2, x: 0, y: 1)
    }
}
