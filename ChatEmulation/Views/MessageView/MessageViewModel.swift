//
//  MessageViewModel.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation

struct MessageViewModel: Identifiable, Hashable {
    typealias ID = String
    var id: String = UUID().uuidString
    
    var text: String
    private var onAppearHandler: (() -> ())?
    
    init(message: Message, onAppear: (() -> ())?) {
        self.text = message.line
        self.onAppearHandler = onAppear
    }
    
    func onAppear() {
        self.onAppearHandler?()
    }
    
    // Identifiable
    static func == (lhs: MessageViewModel, rhs: MessageViewModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    // Hashable
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
