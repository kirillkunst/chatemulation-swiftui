//
//  MessageView.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct MessageView: View {
    @State
    var message: MessageViewModel
    
    var background: some View {
        return Color.gray.opacity(0.25)
    }
    
    var body: some View {
        HStack(alignment: .bottom, spacing: 0) {
            MessageBubbleTriangleView(width: 10, height: 11)
            Text(message.text)
                .padding(10)
                .background(MessageBubbleView())
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .padding([.bottom], 15.0)
        .onAppear {
            self.message.onAppear()
        }
    }
}
