//
//  BubbleRectangleView.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct MessageBubbleTriangleView: View {
    
    var width: CGFloat
    var height: CGFloat
    
    var body: some View {
        Path { path in
            path.move(to: CGPoint(x: 0, y: height * 0.5))
            path.addLine(to: CGPoint(x: width, y: height))
            path.addLine(to: CGPoint(x: width, y: 0))
            path.closeSubpath()
        }
        .fill(Color.bubbleBackground)
        .frame(width: width, height: height)
        .shadow(color: Color.shadowColor.opacity(0.5), radius: 2, x: 0, y: 1)
        .zIndex(10)
        .clipped()
        .padding(.trailing, -1)
        .padding(.leading, 5)
        .padding(.bottom, 5)
    }
}
