//
//  ChatView.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct ChatView: View {
    
    @ObservedObject var viewModel: ChatViewModel

    var body: some View {
        content
        .onAppear { self.viewModel.onAppear() }
        .navigationBarTitle(Text(NSLocalizedString("Dialogue", comment: "Dialogue")),
                            displayMode: .inline)
        
    }
    
    private var content: some View {
        VStack(spacing: 0) {
            Spacer()
            GeometryReader { proxy in
                ScrollView {
                    VStack {
                        ForEach(self.viewModel.messages) { message in
                            MessageView(message: message).flipped().animation(.linear(duration: 0.5))
                            .transition(AnyTransition.messageTransition)
                        }
                    }
                    .frame(width: proxy.size.width, height: proxy.size.height, alignment: .top)
                    .animation(.linear(duration: 0.5))
                    .transition(AnyTransition.messageTransition)
                }
                .frame(width: proxy.size.width, height: proxy.size.height)
                .flipped()
            }
            Spacer()
        }
    }
}

