//
//  ChatViewModel.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

final class ChatViewModel: ObservableObject {
    let messageService: MessagesServiceType
    let speakingService: MessageSpeakingServiceType
    
    @Published var messages: [MessageViewModel] = []
    
    var isAppeared: Bool = false
    
    init() {
        self.speakingService = MessageSpeakingService()
        self.messageService = MessagesService()
        self.speakingService.set(delegate: self)
    }
    
    func onAppear() {
        if isAppeared {
            return
        }
        self.isAppeared = true
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) { [weak self] in
            self?.enqueue()
        }
    }
    
    func enqueue() {
        if let message = self.messageService.fetchNext() {
            let vm = MessageViewModel(message: message, onAppear: { [weak self] in
                self?.speakingService.speak(message: message)
            })
            self.messages.insert(vm, at: 0)
        }
    }
    
}

extension ChatViewModel: MessageSpeakingServiceDelegate {
    func didStartSpeaking() {
        
    }
    
    
    func didFinishSpeaking() {
        self.enqueue()
    }
}
