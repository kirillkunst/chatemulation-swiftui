//
//  FlipModifier.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct FlipModifier: ViewModifier {
    func body(content: Content) -> some View {
        content.scaleEffect(x: 1, y: -1, anchor: .center)
    }
}

extension View {
    func flipped() -> some View {
        self.modifier(FlipModifier())
    }
}
