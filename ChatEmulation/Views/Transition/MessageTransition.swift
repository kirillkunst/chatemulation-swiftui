//
//  MessageTransition.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

extension AnyTransition {
  static var messageTransition: AnyTransition {
    let transition = AnyTransition.move(edge: .top).combined(with: .opacity)
    return transition
  }
}
