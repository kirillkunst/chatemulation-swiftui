//
//  ContentView.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ChatView(viewModel: ChatViewModel())
                .background(Color.customBackground)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
