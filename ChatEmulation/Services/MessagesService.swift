//
//  MessagesLoader.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import Combine

protocol MessagesServiceType {
    func fetchNext() -> Message?
}

class MessagesService: MessagesServiceType {
    
    var messages: Queue<Message> = Queue<Message>()
    
    init() {
        self.load()
    }
    
    private func load() {
        if let path = Bundle.main.path(forResource: "messages", ofType: "json") {
            let url = URL(fileURLWithPath: path)
            
            if let data = try? Data(contentsOf: url),
                let messages = try? JSONDecoder().decode([Message].self, from: data) {
                
                messages.forEach({ self.messages.enqueue(element: $0) })
            }
        }
    }
    
    func fetchNext() -> Message? {
        return self.messages.dequeue()
    }
}
