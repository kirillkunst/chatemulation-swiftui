//
//  MessagesReader.swift
//  ChatEmulation
//
//  Created by Kirill Kunst on 26.07.2020.
//  Copyright © 2020 Kirill Kunst. All rights reserved.
//

import Foundation
import AVFoundation

protocol MessageSpeakingServiceDelegate: class {
    func didStartSpeaking()
    func didFinishSpeaking()
}

protocol MessageSpeakingServiceType {
    func speak(message: Message)
    func set(delegate: MessageSpeakingServiceDelegate)
}

class MessageSpeakingService: NSObject, MessageSpeakingServiceType {
    
    let synthesizer = AVSpeechSynthesizer()
    weak var delegate: MessageSpeakingServiceDelegate?
    
    override init() {
        super.init()
        self.synthesizer.delegate = self
    }
    
    func speak(message: Message) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Constants.delay) { [weak self] in
            let utterance = AVSpeechUtterance(string: message.line)
            self?.synthesizer.speak(utterance)
        }
    }
    
    func set(delegate: MessageSpeakingServiceDelegate) {
        self.delegate = delegate
    }
}

extension MessageSpeakingService: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        self.delegate?.didStartSpeaking()
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Constants.delay) { [weak self] in
            self?.delegate?.didFinishSpeaking()
        }
    }
}


